def cle (chaine:str)->str:
    """renvoie la version trie sans majuscule et caractere accentué de chaine 

    Précondition : aucune 
    Exemple(s) :
    $$$ cle('Orangé')
    'aegnor'
    $$$ cle('é')
    'e'
    $$$ cle('amour')
    'amoru'
    """
    res=[]
    y=bas_casse_sans_accent(chaine)
    for elt in y:
        res.append(elt)
    o=sorted(res)
    ress=''
    for elt in o:
        ress=ress+elt
    return ress


def bas_casse_sans_accent(chaine:str)->str:
    """renvoie lequivalent de chaine sans les majuscules et les caracteres accentues
    Précondition :aucune  
    Exemple(s) :
    $$$ bas_casse_sans_accent('Orangé')
    'orange'
    $$$ bas_casse_sans_accent('mo')
    'mo'


    """
    equiv_non_accentue={'À':'a', 'Â':'a', 'Ä':'a' , 'Ç':'c' ,'É':'e' ,'È':'e' ,'Ê':'e', 'Ë':'e', 'Î':'i' ,'Ï':'i' ,'Ô':'o', 'Ö':'o' ,'Ù':'u', 'Û':'u' ,'Ü':'u', 'Ÿ':'y'
    ,'à':'a', 'â':'a', 'ä':'a', 'ç':'c', 'é':'e', 'è':'e', 'ê':'e' ,'ë':'e' ,'î':'i', 'ï':'i' ,'ô':'o', 'ö':'o', 'ù':'u', 'û':'u', 'ü':'u', 'ÿ':'y'}
    res=''
    for elt in chaine:
        if elt in equiv_non_accentue:
            res=res+equiv_non_accentue[elt]
           
            
        else:
            res= res+ elt.lower()
           
    return res




def recherche_anagramme(mot:str)->list[str]:
    """renvioe tout les anagrammes du mot mot

    Précondition : aucune
    Exemple(s) :
    $$$ recherche_anagramme('orange')
    ['onagre', 'orange', 'orangé', 'organe', 'rongea']
    $$$ recherche_anagramme('info')
    ['foin']
    $$$ recherche_anagramme('Calbuth')
    []
    """
    
    with open('lexique.txt','r') as f:
        mots=f.readlines()
        res=[]
        for elt in mots:
            if cle(mot)==cle(str(elt[:-1])):
                res.append(elt[:-1])
    return res


ANAGRAMMES={mot:recherche_anagramme(mot)}

def anagramme_dictionnaire(mot:str)->list[str]:
    """renvoie les anagrammes du mot mot
    Précondition : aucune 
    Exemple(s) :
    $$$ anagrammes_dictionnaire('orange')
    ['onagre', 'orange', 'orangé', 'organe', 'rongea']
    $$$ anagrammes_dictionnaire('info')
    ['foin']
    $$$ anagrammes_dictionnaire('Calbuth')
    []


    """
    ANAGRAMMES={'mot':recherche_anagramme(mot)}

    return ANNAGRAMMES[mot]
    




            