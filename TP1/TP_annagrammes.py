#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Noms :ALmohamed-alsadou
# Prenoms :fatima
# Groupe :15
# Date :24/01/2024



"""

:mod: module `recursivite`
:author: FIL - Faculté des Sciences et Technologies - Univ. Lille
:link: <http://portail.fil.univ-lille1.fr>_
:date: Mars 2020
:dernière révision: janvier 2024

"""
#méthode split
# les réponses fournies par python pour
# s.split(): ['la','méthode','split','est','parfois','bien','utile'"
# s.split('e'): ['la méthod', 'split', 'st parfois bi','n utile']
# s.split('é'): ['la m','thode split est parfois bien utile']
# s.split(''): erreur seperateur empty
# s.split('split'):['la méthode ', 'est parfois bien utile']
#  
# la fonctionn split renvoie une liste de caracteres sparés par le caractere qu'on spécifie
# oui le la séparant en sous chaine et en supprimons des caracteres

#méthode join
# "".join(l):'laméthodesplitestparfoisbienutile'
# " ".join(l):'la méthode split est parfois bien utile'
# ";".join(l):'la;méthode;split;est;parfois;bien;utile'
# ";".join(l):'latralalaméthodetralalasplittralalaesttralalaparfoistralalabientralalautile'
# print ("\n".join(l)):
# la
# méthode
# split
# est
# parfois
# bien
# utile
# "".join(s):'la méthode split est bien utile'
# "!".join(s):'l!a! !m!é!t!h!o!d!e! !s!p!l!i!t! !e!s!t! !b!i!e!n! !u!t!i!l!e'
# "".join(): erreur 
# "".join([]):''
# "".join([1,2]):erreur
# 
# 
# 2 la méthode join joint des element dune liste ou des caracteres par ce quon lui spécifie
# 
# 
# def join(chaine:str,listechaine:list[str])->str:
#     """renvoie une chaine de caracteres construite en conctaténant toutes les chaines de le en intercalant chaine 
# 
#     Précondition : aucune 
#     Exemple(s) :
#     $$$ join('.', ['raymond', 'calbuth', 'ronchin', 'fr'])
#     'raymond.calbuth.ronchin.fr'
# 
# 
#     """
#     res=''
#     for i  in range(len(listechaine)-1):
#         res+=listechaine[i]+chaine
#     res+=listechaine[-1]
#     return res
# 
# 
# méthode sort
# 1. pour l=list('timoleon') ca ne renvoie rien
# 
# 
# une fonctin sort pour  les chaines
# 
# def sort(chaine:str)->str:
#     """renvoie une chaine de caracteres contenant les caracteres de s tries dans lordre croissant     Précondition : 
#     précondition:aucune 
#     Exemple(s) :
#     $$$ sort('orange')
#     'aegnor'
# 
#     """
#     list=[]
#     res=''
#     alpha=['a','b', 'c' ,'d',' e',' f',' g',' h',' i','j','k','l', 'm', 'n', 'o', 'p', 'q', 'r' ,'s', 't', 'u', 'v', 'w', 'x' ,'y' ,'z'] 
#     for i in range(len(chaine)):
#         for i in range(len(alpha)):
#             if alpha[i]==chaine[i]:
#                 list.append(alpha[i])
#             
#     
#     return res


# def sont_anagrammes_sort(s1: str, s2: str) -> bool:
#     """renvoie True si s1 et s2 sont anagrammatiques , False sinon    Précondition : 
#     Exemple(s) :
#     $$$ sont_anagrammes_sort('orange', 'organe')
#     True
#     $$$ sont_anagrammes_sort('orange','Organe')
#     True 
#     """
#     
#     lists1=list(s1)
#     lists2=list(s2)
#     res=False
#     for i  in range(len(lists1)):
#         if lists1[i]==lists2[i]:
#             res=True
#     return res
# 
# def sont_anagrammes_dict(s1:str,s2:str)->bool:
#     """idem fonction précédente
# 
#     Précondition : 
#     Exemple(s) :
#     $$$ $$$ sont_anagrammes('orange', 'organe')
#     True
#     $$$ sont_anagrammes('orange','Organe')
#     True
# 
# 
#     """
#     compteur=0
#     for elt in s1:
#         for elt2 in s1:
#             if elt==elt2:
#                 compteur=compteur+1
#     return  res 
#         
#         
    
    
    
    


# 
# from ap_decorators import count, trace
# l'instruction suivante permet d'annoter des paramètres qui sont des functions.
# from collections.abc import Callable
# 
# 
# def taille_binaire(naturel: int) -> int:
#     """
#     Renvoie le nombre de chiffres dans l'écriture binaire de l'entier naturel `naturel`
# 
#     Précondition :
#        naturel >= 0
# 
#     Exemples :
#     $$$ taille_binaire(0)
#     1
#     $$$ taille_binaire(1)
#     1
#     $$$ taille_binaire(2)
#     2
#     $$$ taille_binaire(1023)
#     10
#     $$$ taille_binaire(1024)
#     11
#     $$$ from random import randrange
#     $$$ l = [randrange(1,2**100)  for _ in range(100)]
#     $$$ all(taille_binaire(elt) == len(bin(elt))-2  for elt in l)
#     True
# 
#     """
#     res = 1
#     while naturel >= 2:
#         res += 1
#         naturel //= 2
#     return res
# 
# def taille_binaire_recursive(naturel: int) -> int:
#     """
#     Renvoie le nombre de chiffres dans l'écriture binaire de l'entier naturel `naturel`
# 
#     Précondition :
#        naturel >= 0
# 
#     Exemples :
#     $$$ taille_binaire_recursive(0)
#     1
#     $$$ taille_binaire_recursive(1)
#     1
#     $$$ taille_binaire_recursive(2)
#     2
#     $$$ taille_binaire_recursive(1023)
#     10
#     $$$ taille_binaire_recursive(1024)
#     11
#     $$$ from random import randrange
#     $$$ l = [randrange(1,2**100)  for _ in range(100)]
#     $$$ all(taille_binaire_recursive(elt) == len(bin(elt))-2  for elt in l)
#     True
#     """
# #     res = naturel % 2
# #     while naturel > 0:
# #         naturel //= 2
# #         res += naturel % 2
# #     return res
#     if naturel >= 2 :
#         res= 1+taille_binaire_recursive(naturel//2)
#     else:
#         res = 1
#     return res
#         
#     
# 
# def poids_binaire(naturel: int) -> int:
#     """
#     Renvoie le nombre de chiffre 1 dans l'écriture binaire de l'entier naturel `naturel`
# 
#     Précondition :
#        naturel >= 0
# 
#     Exxemples :
# 
#     $$$ poids_binaire(0)
#     0
#     $$$ poids_binaire(1)
#     1
#     $$$ poids_binaire(2)
#     1
#     $$$ poids_binaire(255)
#     8
#     $$$ from random import randrange
#     $$$ l = [randrange(1,2**100)  for _ in range(100)]
#     $$$ all([poids_binaire(x)==bin(x).count('1') for x in l])
#     True
#     """
#     res = naturel % 2
#     while naturel > 0:
#             naturel //= 2
#             res += naturel % 2
#     return res
# 
#    
# 
# def poids_binaire_recursif(naturel: int) -> int:
#     """
#     Renvoie le nombre de chiffre 1 dans l'écriture binaire de l'entier naturel `naturel`
# 
#     Précondition :
#        naturel >= 0
# 
#     Exxemples :
# 
#     $$$ poids_binaire_recursif(0)
#     0
#     $$$ poids_binaire_recursif(1)
#     1
#     $$$ poids_binaire_recursif(2)
#     1
#     $$$ poids_binaire_recursif(255)
#     8
#     $$$ from random import randrange
#     $$$ l = [randrange(1, 2**100)  for _ in range(100)]
#     $$$ all([poids_binaire_recursif(x)==bin(x).count('1') for x in l])
#     True
#     """
#     res=0
#     bit=naturel % 2
#     if naturel > 0:
#         res= bit + poids_binaire_recursif(naturel//2)
#         
#         
#             
#     return res
# 
#     
# 
#         
#         
#         
#   
# 
# def puissance(x: int|float, n: int) -> int|float:
#     """
#     Calcule x élevé à la puissance n
# 
#     Précondition :
#         n>=0
# 
#     Exemples :
# 
#     $$$ puissance(10, 0)
#     1
#     $$$ puissance(10, 1)
#     10
#     $$$ puissance(2, 10)
#     1024
#     """
#     res=0
#     if n>0:
#         res= puissance(x,(n-1))*x
#     else:
#         res = 1
#     return res 
#         
# 
# def puissance_v2(x: int|float, n: int) -> int|float:
#     """
#     calcule  x élevé à la puissance n
# 
#     Précondition :   n>=0
# 
#     Exemples :
#     $$$ puissance_v2(10,0)
#     1
#     $$$ puissance_v2(10,1)
#     10
#     $$$ puissance_v2(2,10)
#     1024
#     """
# #     res=1
# #     
# #     for elt in range(len(n)):
# #         res = fois (x,elt)
# #         
# #     return res
# #
#     res=1
#     if n>0:
#         res=fois(x,x**(n-1))
#         
#     else:
#         res=1
#     return res 
# 
# # @count
# def fois(x: int|float, y: int|float) -> int|float:
#     """
#     renvoie le produit de x par y
# 
#     Précondition : les mêmes que l'opérateur *
# 
#     Exemples :
#     $$$ fois(8, 7)
#     56
#     """
#     return x * y
# 
# def comptage(puissance: Callable[[int|float, int], int|float]) -> list[int]:
#     """
#     Renvoie une liste de longueur 100 contenant le nombre de multiplications
#     effectuées par la fonction ``puissance`` passée en paramètre
# 
#     Précondition :
#        la fonction doit être implantée en utilisant la fonction ``fois``
#     """
#     res = []
#     for i in range(100):
#         fois.counter = 0
#         _ = puissance(2, i)
#         res.append(fois.counter)
#     return res
# #
# #@trace
# def puissance_calbuth(x: int|float, n: int) -> int|float:
#     """
#     calcule  x élevé à la puissance n
# 
#     Précondition :
#         n>=0
# 
#     Exemples :
# 
#     $$$ puissance_calbuth(10,0)
#     1
#     $$$ puissance_calbuth(10,1)
#     10
#     $$$ puissance_calbuth(2,10)
#     1024
# 
#     """
#     if n == 0:
#         return 1
#     if n == 1:
#         return x
#     else:
#         k = n // 2
#         return puissance_calbuth(x, k) * puissance_calbuth(x, n - k)
# 
# def puissance_calbuth_v2(x: int|float, n: int) -> int|float:
#     """
#     calcule  x élevé à la puissance n
# 
#     Précondition :
#         n>=0
# 
#     Exemples :
# 
#     $$$ puissance_calbuth_v2(10,0)
#     1
#     $$$ puissance_calbuth_v2(10,1)
#     10
#     $$$ puissance_calbuth_v2(2,10)
#     1024
# 
#     """
#     ...
# 
# 
# def puissance_calbuth_v2_amelioree(x: int|float, n: int) -> int|float:
#     """
#     calcule  x élevé à la puissance n
# 
#     Précondition :
#         n>=0
# 
#     Exemples :
# 
#     $$$ puissance_calbuth_v2_amelioree(10,0)
#     1
#     $$$ puissance_calbuth_v2_amelioree(10,1)
#     10
#     $$$ puissance_calbuth_v2_amelioree(2,10)
#     1024
# 
#     """
#     ...
# 
# def puissance_erronee(x: int|float, n: int) -> int|float:
#     """
#     aurait dû calculer  x élevé à la puissance n
# 
#     Précondition :
#        n >= 0
# 
#     Exemples :
# 
#     $$$ puissance_erronee(10, 0)
#     1
#     $$$ puissance_erronee(10, 1)
#     10
#     $$$ #$$$ puissance_erronee(2, 10)
#     $$$ #1024
#     """
#     if n == 0:
#         return 1
#     elif n == 1:
#         return x
#     else:
#         r = n % 2
#         q = n // 2
#         return puissance_erronee(x, r) * puissance_erronee(puissance_erronee(x, q), 2)
# 
# def puissance_reparee(x: int|float, n: int) -> int|float:
#     """
#     calcule  x élevé à la puissance n
# 
#     Précondition :
#         n>=0
# 
#     Exemples :
# 
#     $$$ puissance_reparee(10,0)
#     1
#     $$$ puissance_reparee(10,1)
#     10
#     $$$ puissance_reparee(2,10)
#     1024
#     """
#     ...
#     
#     
#     
# def sort_dictionnaire(chaine1:str,chaine2:str)->bool:
#     """ renvoie true si chaine1 et chaine2 sont anagramme
#     Précondition : 
#     Exemple(s) :
#     $$$ sort_dictionnaire('orange','organe')
#     True 
#     $$$ sort_dictionnaire('orange','Orange')
#     False
#     $$$ sort_dictionnaire('orangé','orange')
#     False
#     """
# #     compteur=0
# #     for elt in chaine1:
# #         
# #         for i in range(len(chaine1)):
# #             if chaine1[i]==chaine1[i+1]:
# #                 coompteur=compteur+1
# #         
#     dict1={}
#     for elt in chaine1:
#         dict1[elt]=1
#     dict2={}
#     for elt in chaine2:
#         dict2[elt]=1
#         
#     return dict1==dict2
# 
# def sort_itération_chaine(chaine1:str,chaine2:str)->bool:
#     """renvoie true si les deux chaines sont anagrammes
# 
#     Précondition : 
#     Exemple(s) :
#     $$$ sort_dictionnaire('orange','organe')
#     True 
#     $$$ sort_dictionnaire('orange','Orange')
#     True
#     $$$ sort_dictionnaire('orangé','orange')
#     True
# 
#     """
#     res1=[]
#     cos=set()
#     for elt in chaine1:
#         res.append(elt)
#     for elt in res1:
#         cos.add(res1.count[elt])
#         
#         
#     res2=[]
#     sos=set()
#     for elt in chaine2:
#         res2.append(elt)
#     for elt in res2:
#         sos.add(res2.count[elt])
# 
#     return sos==cos



def bas_casse_sans_accent(chaine:str)->str:
    """renvoie lequivalent de chaine sans les majuscules et les caracteres accentues
    Précondition :aucune  
    Exemple(s) :
    $$$ bas_casse_sans_accent('Orangé')
    'orange'
    $$$ bas_casse_sans_accent('mo')
    'mo'


    """
    equiv_non_accentue={'À':'a', 'Â':'a', 'Ä':'a' , 'Ç':'c' ,'É':'e' ,'È':'e' ,'Ê':'e', 'Ë':'e', 'Î':'i' ,'Ï':'i' ,'Ô':'o', 'Ö':'o' ,'Ù':'u', 'Û':'u' ,'Ü':'u', 'Ÿ':'y'
    ,'à':'a', 'â':'a', 'ä':'a', 'ç':'c', 'é':'e', 'è':'e', 'ê':'e' ,'ë':'e' ,'î':'i', 'ï':'i' ,'ô':'o', 'ö':'o', 'ù':'u', 'û':'u', 'ü':'u', 'ÿ':'y'}
    res=''
    for elt in chaine:
        if elt in equiv_non_accentue:
            res=res+equiv_non_accentue[elt]
           
            
        else:
            res= res+ elt.lower()
           
    return res

def sont_anagrammes(chaine1:str,chaine2:str)->bool:
    """renvoie true si le deux chaines sont les memes sans se soucier des majuscules et des lettres accentues 

    Précondition : aucune 
    Exemple(s) :
    $$$ sont_anagrammes('Orangé', 'organE')
    True
    $$$ sont_anagrammes('à','a')
    True
    $$$ sont_anagrammes('ki','mi')
    False


    """
    res2=[]
    res1=[]
    for elt in chaine1:
        res1.append(elt)
    for c in chaine2:
        res2.append(c)
        
        
    return sorted(bas_casse_sans_accent(res2))==sorted(bas_casse_sans_accent(res1))
        
        
    from lexique import LEXIQUE
    
    
    
    #ANNAGRAMMES: secondes méthode
    
    
     #il nest pas raisonnable de prendr les mots du lexique pour cle car plusiers mots du lexique peuvent avoir les memes anagrammes or les cles doivent etre unique 

def cle (chaine:str)->str:
    """renvoie la version trie sans majuscule et caractere accentué de chaine 

    Précondition : aucune 
    Exemple(s) :
    $$$ cle('Orangé')
    'aegnor'
    $$$ cle('é')
    'e'
    $$$ cle('amour')
    'amoru'
    """
    res=[]
    y=bas_casse_sans_accent(chaine)
    for elt in y:
        res.append(elt)
    o=sorted(res)
    ress=''
    for elt in o:
        ress=ress+elt
    return ress

#anagramme dun mot premiere methode , petit 1 (faire une fonction qui rechercher les anagrammes dun mot donné)
def recherche_anagramme(mot:str)->list[str]:
    """renvioe tout les anagrammes du mot mot

    Précondition : aucune
    Exemple(s) :
    $$$ anagrammes('orange')
    ['onagre', 'orange', 'orangé', 'organe', 'rongea']
    $$$ anagrammes('info')
    ['foin']
    $$$ anagrammes('Calbuth')
    []
    """
    res=[]
    with open('LEXIQUE.txt','r') as f:
        
        for line in f:
            if cle(f.readline())==cle(mot):
                res.append(line)
    return res 
            

def recherche_anagramme(mot:str)->list[str]:
    """renvioe tout les anagrammes du mot mot

    Précondition : aucune
    Exemple(s) :
    $$$ recherche_anagramme('orange')
    ['onagre', 'orange', 'orangé', 'organe', 'rongea']
    $$$ recherche_anagramme('info')
    ['foin']
    $$$ recherche_anagramme('Calbuth')
    []
    """
    
    with open('lexique.txt','r') as f:
        mots=f.readlines()
        res=[]
        for elt in mots:
            if cle(mot)==cle(str(elt[:-1])):
                res.append(elt[:-1])
    return res
            
#les anagrammes de chiens sont chien chiné , niche , niché 



ANAGRAMMES={mot:recherche_anagramme(mot)}

def anagramme_dictionnaire(mot:str)->list[str]:
    """renvoie les anagrammes du mot mot
    Précondition : aucune 
    Exemple(s) :
    $$$ anagrammes_dictionnaire('orange')
    ['onagre', 'orange', 'orangé', 'organe', 'rongea']
    $$$ anagrammes_dictionnaire('info')
    ['foin']
    $$$ anagrammes_dictionnaire('Calbuth')
    []


    """
    ANAGRAMMES={'mot':recherche_anagramme(mot)}

    return ANNAGRAMMES[mot]
    
            
            
    





        
        
    
    
    
    
    
    

