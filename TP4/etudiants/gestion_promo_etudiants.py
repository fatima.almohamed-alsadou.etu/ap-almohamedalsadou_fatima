#all
def pour_tous(l:list[bool])->bool:
    """renvoie true si et seulement si touts les elements de list l veulent true

    Précondition : aucune 
    Exemple(s) :
    
    $$$ pour_tous([])
    True
    $$$ pour_tous([True, True, True])
    True
    $$$ pour_tous([True, False, True])
    False
    """
    res=True
    for elt in l:
        if elt==False:
            return False
    return res 
#any     
def il_existe(seq_bool: list[bool]) -> bool:
    """

    Renvoie True si seq_bool contient au moins une valeur True, False sinon

    Exemples:

    $$$ il_existe([])
    False
    $$$ il_existe([False, True, False])
    True
    $$$ il_existe([False, False])
    False
    """
    
    for elt in seq_bool:
        if elt==True:
            return True
    return False

    
    