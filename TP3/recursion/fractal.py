import turtle


def zigzag():
    """à_remplacer_par_ce_que_fait_la_fonction

    Précondition : 
    Exemple(s) :
    $$$ 

    """
    
for i in range(3):
    
    turtle.left(60)
    turtle.forward(50)
    turtle.right(120)
    turtle.forward(50)
    turtle.left(120)
    turtle.forward(50)
    turtle.right(120)
    turtle.forward(50)
    turtle.left(60)
    
    
 #si la longueur de tracé l a lordre n est l alors a l'ordre n
   
# a lordre n on a un trace de longueuer l
# 
# a lordre n+1 le segment a tracer est divisé en trois segments egaux
# donc en trois segments de longeuer l*(1/3)
# ensuite dans le segement du du milieu on la remplace par 2 cotes dun triangle equilatéral de cote l*(1/3)
# 
# finalement le trace est compose de 3 portion , la premiere portion de longeuer l*(1/3) et la portion du milieu de longeur 2*l*(1/3) et une derniere portion de longeuer l*(1/3)
# 
# donc a lordre n+1 le tracé est de longeuer l+l*(1/3)
      
def von_koch(l:int|float,n:int)->int:
    """dessine la courbe de von koch d'ordre n
    Précondition : 
    Exemple(s) :
    $$$ 

    """
    if n==0:
       turtle.forward(l)
    else:
       von_koch(1/3,n-1)
       turtle.left(60)
       von_koch(1/3,n-1)
       turtle.right(120)
       von_koch(1/3,n-1)
       turtle.left(60)
       von_koch(1/3,n-1)
       
        
    
    
        
        

