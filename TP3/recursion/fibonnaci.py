# f2=1
# f3=2
# f4=3
# f5=5
# f6=9
# f7=14
# f8=23
# f9=37
# f10=60

from ap_decorators import count 
@count
def fibo (n:int)->int :
    """
    renvoie le terme f(n) de la suite de fibonacci 

    Précondition : aucune
    Exemple(s) :
    $$$ fibo(6)
    8
    $$$ fibo(2)
    1
    $$$ fibo(10)
    55

    """
    
    
    if n<=1:
        res=n
    else:
        
        res=fibo(n-1)+fibo(n-2)
    return res 
    
#fibo(40)= 102334155 la suite croit vite 
        
    
    
    
