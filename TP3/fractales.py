
from ap_decorators import trace

@trace
def somme_rec(a,b)->int:
    """
renvoie la somme deux a et b par récursivité

    Précondition :a>=0  
    Exemple(s) :
    $$$ somme_rec(10,5)
    15
    $$$ somme_rec(2,3)
    5

    """
    if a>0:
        res=somme_rec(a-1,b+1)
    else:
         res=b
    return res


def binomial(n,p)->int:
    """
    renvoie coef binomiale de n et p 
    Précondition : n>=p>=0
    Exemple(s) :
    $$$ binomial(5,0)
    1
    $$$ binomial(6,1)
    6
    

    """
    res=''
    if p==0 or n==p:
        res=1
    else:
        res= binomial(n-1,p-1) + binomial(n-1,p)
    return res
        
@trace    
def is_palindromic(mot:str):
    """renvoie true si mot est un palindrome 
    Précondition : len(mot) non nul
    Exemple(s) :
    $$$ is_palindromic('été')
    True
    $$$ is_palindromic('fraise')
    False
    $$$ is_palindromic('elle')
    True

    """
    
    
    res=False
    if len(mot)<2:
       res=True
    
    elif mot[0]==mot[-1]:
        x=mot[1:-1]
        res=is_palindromic(x)
    else:
        res=False
    return res

        
    
    
    
    
    
    
    