#[K*n for k in range(0,n)]

from random import shuffle

def liste_alea(n:int):
    """
    construit une liste de longueuer n contenant les entiers de 0 a n-1 melanges

    Précondition : 
    Exemple(s) :
   

    """
    #shuffle(liste) renvoie None
    L=[ k for k in range(n)]
    shuffle(L)
    return L
    
    
    
#     
#     
#     
# 
# import matplotlib.pyplot as plt
# 
# timeit.timeit(stmt='tri_select(liste_alea(TAILLE_MAX=100),comp)',"from math import", number=5000)


