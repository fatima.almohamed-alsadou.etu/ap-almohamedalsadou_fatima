class Bloc :
    """

    $$$ bloc1=Bloc((3,1),(10,2),(0,0,255),4)
    
    $$$ bloc1.pxhautgauche
    (3,1)
    $$$ bloc1.pxbasdroite
    (10,2)
    $$$ bloc1.couleur
    (0,0,255)
    $$$ bloc1.sousblocs
    4
    $$$ bloc2=Bloc((0,8),(8,0),(0,0,255),0)
    $$$ bloc2.decoupage_sous_blocs()
    [(0,8),(4.0,4.0),(4.0,8),(8,4.0),(0,4.0),(4.0,0),(4.0,4.0),(8,0)]
    """
    
    
    
    
    
    def __init__(self,pxhautgauche:tuple[int,int],pxbasdroite:tuple[int,int],couleur:tuple[int,int,int],sousblocs:int):
             
             self.pxhautgauche=pxhautgauche
             self.pxbasdroite=pxbasdroite
             self.couleur=couleur
             self.sousblocs=sousblocs
             
             
             
    def est_uniforme(self)->bool:
        """renvoie True si le bloc propiétaire de cette methode est uniforme , cad qu'il ne contient pas de sous blocs

        Précondition : 
        Exemple(s) :
        $$$ 

        """
        return self.sousblocs==0
    
        
        
    def decoupage_sous_blocs(self)->list[tuple[int,int],tuple[int,int],tuple[int,int],tuple[int,int]] :
        """decouper le bloc proprétaire de cette methode en quatre sous blocs et renvoie une liste contenant quatre elements et chaque element contient les coordonnées du pixelhautgauche et pixelbasdroite de chaque sous blocs découpés
        """
        return[((0,self.pxhautgauche[1])\
               ,(self.pxbasdroite[0]/2,self.pxhautgauche[1]/2))\
               ,((self.pxbasdroite[0]/2,self.pxhautgauche[1])\
               ,(self.pxbasdroite[0],self.pxhautgauche[1]/2))\
               ,((0,self.pxhautgauche[1]/2)\
               ,(self.pxbasdroite[0]/2,0))\
               ,((self.pxbasdroite[0]/2,self.pxhautgauche[1]/2)\
               ,(self.pxbasdroite[0],0))]
        
        
        
        

        
    def __str__(self)->str:
        return f"({self.pxhautgauche},{self.pxbasdroite},{self.couleur},{self.sousblocs})"
        
      
    def __repr__(self)->'Bloc':
        
        return Bloc({self.pxhautgauche},{self.pxbasdroite},{self.couleur},{self.sousblocs})
        
             
             
    
     
     
             
     
     