def calcule_moyenne_couleur(*couleur:tuple[int,int,int])->tuple[int,int,int]:
    """renvoie la couleur moyenne d'une liste de couleur

    Précondition : couleur sous forme de rvb
    Exemple(s) :
    $$$ 

    """
    R=0
    V=0
    B=0
    for i in range(len(couleur)):
        R+=(couleur[i][0])
        
    for i in range(len(couleur)):
        V+=(couleur[i][1])
    for i in range(len(couleur)):
        B+=(couleur[i][2])
    
    resR=R/len(couleur)
    resV=V/len(couleur)
    resB=B/len(couleur)
    
    return (resR,resV,resB)
    
    