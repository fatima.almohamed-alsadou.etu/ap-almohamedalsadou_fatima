def est_couleur_proche(col1:tuple[int,int,int],col2:tuple[int,int,int])->bool:
    """renvoie True si col1 et  col2 sont proches visuellement ,cad si la défférence de chacune de leur composantes ne dépasse pas un certain seuil

    Précondition : aucune
    Exemple(s) :
    $$$ ((50,100,30),(200,100,30))
    False
    $$$ ((250,100,30),(240,90,60))
    True
    """
    seuil=40
    return (abs(col1[0]-col2[0]) <= seuil) and ((col1[1]-col2[1]) <= seuil) and (abs(col1[2]-col2[2]) <=seuil)
