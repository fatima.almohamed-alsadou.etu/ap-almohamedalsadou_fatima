from classbloc import Bloc
from calcul_couleur_moyenne import *
from coord_blocs import *
from est_couleur_proche import *
from PIL import Image, ImageDraw
def image_recursive(image:str,ordre:int):
    """represente limage image selon l'ordre passe en paramétre

    Précondition : aucune
    Exemple(s) :
    $$$ 

    """
    #si l'image passé en paramètre est une image png
    if 'png' in image:
        imageinitiale=Image.open(image)
        dim_image=imageinitiale.size
        if ordre>0:
                division=Bloc((0,dim_image[0]),(dim_image[0],0),None,0)
                dim_quatresousblocs=division.decoupage_sous_blocs()
                division.sousblocs+=4
                
                for i in range(len(dim_quatresousblocs)):
                    image_recursive('(({dim_quatresousblocs[i][0]}),({dim_quatresousblocs[i][1]}))',ordre-1)
                        
            
        
    #si image passé en paramètre est un bloc et non une image    
    else:
        #renvoie une liste des coordonnées pixelhautgauche et pixelbasdroite du bloc passé en paramètre en image_recursive()
        coord_pixel=coord_str(image)
        if ordre>0:
                division=Bloc((coord_pixel[0],coord_pixel[1]),(coord_pixel[2],coord_pixel[3]),None,0)
                dim_quatresousblocs=division.decoupage_sous_blocs()
                division.sousblocs+=4
        
                for i in range(len(dim_qautresousblocs)):
                    image_recursive(str((dim_qautresoublocs[i][i]),(dim_qautresoublocs[i][i+1])),ordre-1)
        
    
    
            #calcule couleur moyenne de chaque blocs pour ensuite déterminer s'il sont de couleur proche
            #on parcours les pixels de chaque bloc a l'aide de getpixel et ensuite on calcule la couleur moyenne a l'aide de calcule_couleur_moyenne()
                    
                couleurs_blocs=[]#liste contentant la couleur moyenne de chacun des quatres sous blocs
                RVB_bloc=[]#liste contentant tout les couleurs d'un blocs
                for i in range(4):
                        for x in (min(dim_quatresousblocs[i][0][0],dim_quatresousblocs[i][1][0]),max(dim_quatresousblocs[i][0][0],dim_quatresousblocs[i][1][0])):
                            for y in (min(dim_quatresousblocs[i][1][1],dim_quatresousblocs[i][1][1]),max(dim_quatresousblocs[i][1][1],dim_quatresousblocs[i][1][1])):
                                RVB_bloc.append(imageinitiale.getpixel(x,y))
                        
                        #calcule de la couleur moyenne d'un bloc
                        moyenne_rec=(0,0,0)
                        for elt in RVB_bloc:
                            moyenne_rec=calcule_moyenne_couleur(elt,moyenn_rec)
                        
                        couleurs_blocs.append(moyenne_rec)
                 
                #fin calcule couleur moyenne des blocs, la couleur moyenne des chacun des blocs est contenue dans la liste couleur_bloc
                constante_bool=[]
                for elt in couleurs_blocs:
                    constante_bool.append(est_couleur_proche(elt,couleurs_blocs[1]))
                    #si les quatres couleur sont de couleurs proches
                    if all(contante_bool):
                        #calcule couleur moyenne quatre blocs
                        couleurmoyenne_quatreblocs=calcule_moyenne_couleur(couleurs_blocs[0],couleurs_blocs[1],couleurs_blocs[2],couleurs_blocS[3])
                        
                        for x in (min(coord_blocs[0][0],coord_blocs[1][0]),max(coord_blocs[0][0],coord_blocs[1][0])):
                            for y in (min(coord_blocs[0][1],coord_blocs[1][1]),max(coord_blocs[0][0],coord_blocs[1][1])):
                                imageinitiale.putpixel((x,y),couleurmoyenne_quatreblocs)
                    #sinon(les quatres blocs ne sont pas de couleur proches)  
                    #else:
                        
        #si l'ordre est nul:
        #else:
            
            