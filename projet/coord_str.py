def coord_str(strbloc:str)->list:
    """retourne les coordonnees des une chaine de caractere  sous la forme dune liste

    Précondition : 
    Exemple(s) :
    $$$ '((3,0),(5,99))'
    [3,0,5,99]
    $$$ '((999,1),(45,99))'
    [999,1,45,99]
    """
    
    res=''
    coord=[]
    for i in range(len(strbloc)):
             if strbloc[i].isdigit():
                res+=strbloc[i]
             elif res!='':
                coord.append(int(res))
                res=''
            
    return coord
